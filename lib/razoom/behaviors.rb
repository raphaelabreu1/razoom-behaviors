require 'active_model'
require 'active_support/core_ext'
require 'razoom/behaviors/version'
require 'razoom/behaviors/attributable'
require 'razoom/behaviors/persistable'

module Razoom
  module Behaviors

  end
end

# core_ext
class String
  def plural?
    self.pluralize == self
  end
end
