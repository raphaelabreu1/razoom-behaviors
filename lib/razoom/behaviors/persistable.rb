module Razoom
  module Behaviors
    module Persistable
      extend ActiveSupport::Concern

      included do
        attr_accessor :id

        def persisted?
          id.present?
        end
      end
    end
  end
end