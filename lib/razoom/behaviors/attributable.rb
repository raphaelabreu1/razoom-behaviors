module Razoom
  module Behaviors
    module Attributable
      extend ActiveSupport::Concern

      included do |klass|
        def klass.association(name, options={})
          self.send(:define_method, "#{name}=") do |params|
            assign_association(name.to_s, params, options)
          end

          self.send(:define_method, name) do
            get_association(name.to_s, options)
          end
        end

        def klass.validates_associated(*attr_names)
          validates_with AssociatedValidator, _merge_attributes(attr_names)
        end

        public
        def assign_attributes(params)
          params.each do |attr, value|
            self.public_send("#{attr}=", value)
          end if params
        end

        def method_missing(method_sym, *arguments, &block)
          # TODO instance_variable_set "@#{method_sym}", arguments
          eval("@#{method_sym} = '#{arguments}'") if arguments.any?
        rescue SyntaxError
          p "Não foi encontrado uma forma de atribuir #{method_sym} com #{arguments.to_s}"
        end

        def as_param
          { :"#{self.class.model_name.param_key}" => self.as_json }.to_json
        end

        private
        def assign_association(association_name, params, options)
          association_class_name = options[:class_name] || association_name.classify
          association_class = eval("#{association_class_name}")

          if params.is_a?(Array)
            value_of_association = params.map do |hash_or_object|
              if hash_or_object.is_a?(Hash)
                association_class.new(hash_or_object)
              else
                hash_or_object
              end
            end
          elsif params.is_a?(Hash)
            value_of_association = association_class.new(params)
          else
            value_of_association = params
          end

          instance_variable_set("@#{association_name}", value_of_association)
        end

        def get_association(association_name, options)
          association = instance_variable_get("@#{association_name}")

          if association.nil? and options[:autobuild]
            if association_name.plural?
              association = assign_association(association_name, [], options)
            else
              association = assign_association(association_name, {}, options)
            end
          end
          association
        end
      end

      class AssociatedValidator < ActiveModel::EachValidator
        def validate_each(record, attribute, value)
          if Array.wrap(value).reject {|r| r.marked_for_destruction? || r.valid?}.any?
            record.errors.add(attribute, :invalid, options.merge(:value => value))
          end
        end
      end
    end
  end
end
