require 'spec_helper'

describe Razoom::Behaviors::Attributable do
  it 'should initialize an object with association' do
    model = AssociatedModel.new(id: 1)
    model.model_attribute = AssociatedModel.new(id: 2)

    expect(model).to respond_to(:model_attribute)
    expect(model).to respond_to(:model_attribute=)
    expect(model.id).to eq(1)
    expect(model.model_attribute.id).to eq(2)
  end

  it 'should assign many attributes' do
    model = ModelAttribute.new
    model.assign_attributes({id: 1, name: 'Test Model', test_attr: 'AttributableBehavior'})
    expect(model.id).to eq(1)
    expect(model.name).to eq('Test Model')
    expect(model.test_attr).to eq('AttributableBehavior')
  end

  it 'should render as json with children' do
    model = AssociatedModel.new(id: 1)
    model.model_attribute = ModelAttribute.new(id: 2)

    expect(model.as_param).to eq({associated_model: {id: 1, model_attribute: {id: 2}}}.to_json)
  end

  it 'should autobuild associated' do
    model = AutobuildModel.new(id: 1)
    expect(model.model_attribute).to_not eq(nil)
  end

  it 'should not autobuild associated' do
    model = AssociatedModel.new(id: 1)
    expect(model.model_attribute).to eq(nil)
  end

  it 'should validate association with model' do
    model = AssociatedModel.new(id: 1)
    expect(model.valid?).to eq(false)
  end

  it 'should validate model with children model' do
    model = ValidateAssociation.new(id: 1)
    model.model_attribute = ModelAttribute.new()
    expect(model.valid?).to eq(false)
    model.model_attribute.id = 2
    expect(model.valid?).to eq(true)
  end

  it 'should initialize an object with array association' do
    model = ManyModel.new(id: 1)
    model.model_attributes = [ModelAttribute.new(id: 2), ModelAttribute.new(id: 3)]

    expect(model).to respond_to(:model_attributes)
    expect(model).to respond_to(:model_attributes=)
    expect(model.id).to eq(1)
    expect(model.model_attributes.map(&:id)).to include(2, 3)
    expect(model.model_attributes[0].class).to eq(ModelAttribute)
  end

  it 'should initialize an object with array association by hash' do
    model = ManyModel.new({id: 1, model_attributes: [{id: 2}, {id: 3}]})

    expect(model).to respond_to(:model_attributes)
    expect(model).to respond_to(:model_attributes=)
    expect(model.id).to eq(1)
    expect(model.model_attributes.map(&:id)).to include(2, 3)
    expect(model.model_attributes[0].class).to eq(ModelAttribute)
  end

  it 'should return an empty array when association is array' do
    model = ManyModel.new({id: 1})
    expect(model.model_attributes).to eq([])
  end

  it 'should initialzie an object with a specific class_name in association' do
    model = SpecificModel.new(id: 1)
    expect(model.model_attributation.class).to eq(ModelAttribute)
  end

  it 'should set an attribute that has specific class_name' do
    model = SpecificModel.new(id: 1)
    model.model_attributation = ModelAttribute.new(id: 2)
    expect(model.model_attributation.class).to eq(ModelAttribute)
    expect(model.model_attributation.id).to eq(2)
  end
end
