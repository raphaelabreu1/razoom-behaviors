require 'spec_helper'

describe Razoom::Behaviors::Persistable do
  it 'should return a unpersisted object' do
    expect(ModelPersistable.new.persisted?).to eq(false)
  end

  it 'should return a persisted object' do
    expect(ModelPersistable.new(id: 1).persisted?).to eq(true)
  end

  it 'should return a persisted object after update attributes' do
    model = ModelPersistable.new
    model.name = 'Test Model'
    model.id = 1
    expect(model.persisted?).to eq(true)
  end
end