class AssociatedModel
  include ActiveModel::Model
  include Razoom::Behaviors::Attributable

  attr_accessor :id
  association :model_attribute

  validates :model_attribute, presence: true
end

class ModelAttribute
  include ActiveModel::Model
  include Razoom::Behaviors::Attributable
  attr_accessor :id, :name, :test_attr

  validates :id, presence: true
end

class ModelPersistable
  include ActiveModel::Model
  include Razoom::Behaviors::Persistable

  attr_accessor :name
end

class AutobuildModel < AssociatedModel
  association :model_attribute, autobuild: true
end

class ValidateAssociation < AssociatedModel
  association :model_attribute
  validates_associated :model_attribute
end

class ManyModel < AssociatedModel
  association :model_attributes, autobuild: true
end

class SpecificModel < AssociatedModel
  association :model_attributation, class_name: 'ModelAttribute', autobuild: true
end
