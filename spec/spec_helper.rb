$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'razoom/behaviors'
require 'razoom/behaviors/version'
require 'razoom/behaviors/attributable'
require 'razoom/behaviors/persistable'
require_relative 'razoom/support/models'