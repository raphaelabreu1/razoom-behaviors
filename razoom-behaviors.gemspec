# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'razoom/behaviors/version'

Gem::Specification.new do |spec|
  spec.name          = 'razoom-behaviors'
  spec.version       = Razoom::Behaviors::VERSION
  spec.authors       = ['Raphael Abreu']
  spec.email         = ['raphaelabreu1@gmail.com']

  spec.summary       = %q{Gem com os comportamentos dos modelos da Razoom}
  spec.homepage      = 'https://bitbucket.org/raphaelabreu1/razoom-behaviors/'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'bundler', '~> 1.8'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rails', '~> 4.1.1'
end
